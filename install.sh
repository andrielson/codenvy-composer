#!/bin/bash

add-apt-repository --yes ppa:ondrej/php;
apt-get -y --allow-unauthenticated update;
apt-get -qqy --allow-unauthenticated install php7.3-cli;

EXPECTED_SIGNATURE="$(wget -q -O - https://composer.github.io/installer.sig)";
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');";
ACTUAL_SIGNATURE="$(php -r "echo hash_file('sha384', 'composer-setup.php');")";

if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]
then
    >&2 echo 'ERROR: Invalid installer signature';
    rm composer-setup.php;
    exit 1;
fi
php composer-setup.php --install-dir=/usr/local/bin --filename=composer && php -r "unlink('composer-setup.php');";
chown -R user:user ~/.composer;
echo 'Use a opção "composer require --ignore-platform-reqs ... para baixar as dependências ignorando as bibliotecas necessárias do PHP"';